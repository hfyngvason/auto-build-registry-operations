package main

import (
	"log"
	"os"

	"github.com/google/go-containerregistry/pkg/authn"
	"github.com/google/go-containerregistry/pkg/name"
	"github.com/google/go-containerregistry/pkg/v1/mutate"
	"github.com/google/go-containerregistry/pkg/v1/remote"
)

func main() {
	cmd := os.Args[1]
	imageRefStr := os.Args[2]

	ref, err := name.ParseReference(imageRefStr, name.WeakValidation)
	if err != nil {
		log.Fatalf("Failed to parse image ref: %v\n", err)
	}

	auth, err := authn.DefaultKeychain.Resolve(ref.Context().Registry)
	if err != nil {
		log.Fatalf("Failed to authenticate: %v\n", err)
	}

	img, err := remote.Image(ref, remote.WithAuth(auth))
	if err != nil {
		log.Fatalf("Failed to retrieve image: %v\n", err)
	}

	option := remote.Option(remote.WithAuth(auth))

	if cmd == "set-env" {
		configFile, err := img.ConfigFile()
		if err != nil {
			log.Fatalf("Failed to read image Config: %v\n", err)
		}

		config := configFile.Config.DeepCopy()
		config.Env = append(config.Env, "PORT=5000")

		img, err = mutate.Config(img, *config)
		if err != nil {
			log.Fatalf("Failed to mutate local image config: %v\n", err)
		}

		err = remote.Write(ref, img, option)
		if err != nil {
			log.Fatalf("Failed to write image to registry: %v\n", err)
		}
	} else if cmd == "tag" {
		newTagStr := os.Args[3]

		descriptor, err := remote.Get(ref, option)
		if err != nil {
			log.Fatalf("Failed to retrieve descriptor: %v\n", err)
		}

		destination := ref.Context().Tag(newTagStr)
		err = remote.Tag(destination, descriptor, option)
		if err != nil {
			log.Fatalf("Failed to write tag to registry: %v\n", err)
		}
	} else {
		log.Fatalln("Unrecognized command:", cmd)
	}
}
